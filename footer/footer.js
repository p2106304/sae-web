const template = document.createElement('template');
template.innerHTML = `
<style>


.carte {
    position: absolute;
    top: 20px;
    left: 30px;
    padding: 15px;
    padding-left: 25px;
    padding-right: 25px;
    color: #ffffff;
    background: #0095d6;
    font-family: "Montserrat", serif;
    border-radius: 25px;
    font-size: 14px;
}

.logo-2 {
    width: 90px;
    height: 100%;
    cursor: pointer;
    margin-right: auto;
}


footer {
    background-color: #42628c;
    padding-top: 20px;
    padding-bottom: 20px;
}

.container-partenaires {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    overflow-wrap: break-word;
    grid-gap: 16px;
    padding: 15px;
    position: relative;
}


.img-part1 {
    height: 31%;
    width: 26%;
    padding-top: 8px;
    padding-left: 35px;
}

.img-part2 {
    width: 35%;
    padding-left: 26px;
    padding-bottom: 24px;

}

footer>.container-partenaires>iframe {
    margin-left: 10px;
    border-radius: 25px;
    padding-bottom: 15px;

}

.map {
    width: 900px;
    height: 550px;
    border-style: none;
}

.sinscrire{
    font-family: "Montserrat", serif;
}


.titre{

font-size: 32px;
text-align: center;
padding-top: 70px;

}

.commentaire {
    
    font-size: 18px;
    line-height: 55px;
    text-align: center;
    padding-top: 70px;
    padding-left: 150px;  
    padding-right: 150px;  
}
.commentaire > a:hover {
    text-decoration: underline;

}
.commentaire > a {
    color: black;
    
}

.container-reseau {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    padding-top: 20px;
    padding-bottom: 10px;
    border-top: solid;
}

.container-reseau>a>div {
    font-family: "Montserrat", serif;
    font-size: 22px;
    color: rgb(15, 13, 13);
}

.facebook>.bx {
    color: rgb(23, 114, 234);
}

.instagram>.bx {
    color: black;
}

.Twitter>.bx {
    color: rgb(28, 156, 235);
}

.Youtube>.bx {
    color: rgb(247, 0, 0);
}

.Gitlab>.bx {
    color: rgb(219, 65, 40);
}

a {
    color: aliceblue;
    text-decoration: none;
}


@media (max-width:1630px) {

    .commentaire {
        
        font-size: 16px;
        line-height: 55px;
        padding-top: 50px;
        text-align: start;
        padding-top: 40px;
        padding-left: 10px;  
        padding-right: 10px;  
    }

}


@media (max-width:1450x) {

    .commentaire {
        
        font-size: 16px;
        line-height: 45px;
        padding-top: 50px;
        text-align: start;
        padding-top: 40px;
        padding-left: 10px;  
        padding-right: 10px;  
    }

}

@media (max-width:1295px) {

    .container-partenaires {
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        overflow-wrap: break-word;
    }

    .sinscrire{
    grid-row-start: 2;
    .commentaire {
        
        font-size: 16px;
        line-height: 45px;
        padding-top: 50px;
        text-align: center;
        padding-top: 40px;
        padding-left: 10px;  
        padding-right: 10px;  
    }


    }
    
    
        .map {
            width: 1150px;
            height: 550px;
            border-style: none;
        }

     
    }

    @media (max-width:1200px) {
    .map {
        width: 950px;
        height: 550px;
        border-style: none;
    }
}

@media (max-width:1000px) {

    

    .map {
        width: 850px;
        height: 550px;
        border-style: none;
    }

    .carte {
        position: absolute;
        top: 20px;
        left: 30px;
        padding: 12px;
        padding-left: 25px;
        padding-right: 25px;
        font-size: 13px;
    }
}
@media (max-width:870px) {


    .map {
        width: 700px;
        height: 550px;
        border-style: none;
    }
}



@media (max-width:850px) {

    .container-reseau>a>div {
        font-size: 16px;
    }

    .container-reseau {
        display: flex;
        justify-content: safe center;
        flex-wrap: wrap;
        padding-top: 20px;
        padding-bottom: 10px;
        border-top: solid;
    }
    
}

@media (max-width:750px) {


    .map {
        width: 600px;
        height: 450px;
        border-style: none;
    }

    .carte {
        position: absolute;
        top: 20px;
        left: 30px;
        padding: 10px;
        padding-left: 25px;
        padding-right: 25px;
    }
    .logo-2 {
        width: 50px;
        height: 100%;
        cursor: pointer;
        margin-right: auto;
    }
    
    
}
@media (max-width:650px) {

   
    .container-reseau {
        display: flex;
        justify-content: flex-start;;
        flex-wrap: wrap;
        padding-top: 20px;
        padding-bottom: 10px;
        border-top: solid;
    }

    .map {
        width: 550px;
        height: 350px;
        border-style: none;
    }
}

@media (max-width:600px) {

   
    .map {
        width: 450px;
        height: 350px;
        border-style: none;
    }

}





</style>


    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>


<footer>

    <article class="container-partenaires">

        <iframe class="map"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.4721156001915!2d4.880790290484198!3d45.78607792597958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4c01bcfcd4551%3A0xc63aa9b2af06af29!2sD%C3%A9partement%20informatique%20de%20l&#39;IUT%20Lyon%201!5e0!3m2!1sfr!2sfr!4v1638194579172!5m2!1sfr!2sfr">
        </iframe>
            
        <div class="carte">
            <img class="logo-2" src="../univ-lyon1-avec-du-blanc.png" alt="logo Lyon 13">

            <div class="numero">
                Tel Accueil : 04.72.69.20.00 <br>
                Fax : 04.72.69.20.94
            </div>

            <div class="adresse">
                IUT Lyon13 Site de Villeurbanne La Doua <br>
                1 Rue de la technologie <br>
                69100 Villeurbanne
            </div>

        </div>

        
        <section class="sinscrire">
            <div class="titre">
                S'inscrire à Lyon 13
            </div>
            
          <div class="commentaire">
          <a href='../formulaire/formulaire.html' > Vous avez la possibilité de vous inscrire à Lyon 13 afin de recevoir des informations supplémentaires sur toutes les formations proposées.
          En vous inscrivant, vous aurez accès au serveur Discord de l'IUT où vous pourrez poser des questions aux étudiants de l'école.
                </a>
            </div>
         </section>
     

        

    </article>


    <article class="container-reseau">

        <a href="https://www.facebook.com/UnivLyon1 " target="_blank">
            <div class="facebook"> <i class='bx bxl-facebook-circle'></i> facebook </div>
        </a>
        <a href="https://www.instagram.com/univlyon1/ " target="_blank">
            <div class="instagram"> <i class='bx bxl-instagram'></i> instagram </div>
        </a>
        <a href="https://twitter.com/UnivLyon1 " target="_blank">
            <div class="Twitter"> <i class='bx bxl-twitter'></i> Twitter </div>
        </a>
        <a href=" https://www.youtube.com/UnivLyon1" target="_blank">
            <div class="Youtube"> <i class='bx bxl-youtube'></i> Youtube </div>
        </a>
        <a href=" https://forge.univ-lyon1.fr/" target="_blank">
            <div class="Gitlab"> <i class='bx bxl-gitlab'></i> Gitlab </div>
        </a>
        
    </article>

</footer>`

class customFooter extends HTMLElement {
    constructor() {
        super();

        const shadowRoot = this.attachShadow({ mode: 'closed' });
        shadowRoot.appendChild(template.content.cloneNode(true));
    }
}

customElements.define('custom-footer', customFooter);